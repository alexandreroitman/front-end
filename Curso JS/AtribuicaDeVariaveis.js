console.log("Atribuição de Variaveis");

const idade = 22; //const não pode variar 
let primeiroNome = "Alexandre"; //let pode variar
const sobrenome = "Roitman";

console.log(`Meu nome é ${primeiroNome} ${sobrenome}`);
console.log(primeiroNome, sobrenome);

primeiroNome = primeiroNome + sobrenome;
console.log(primeiroNome);



